# sim-ckan

Para instalar Ckan, siga los pasos que se indican a continuación:

PASO 1. PREPARAR LOS PAQUETES NECESARIOS.

1. INSTALAR PAQUETES
	sudo apt-get update
	sudo apt-get install python-dev postgresql libpq-dev python-pip         python-virtualenv git-core solr-jetty openjdk-8-jdk redis-server
	sudo apt-get install python-venv
	opción con python3: sudo apt-get install python3-dev postgresql libpq-dev python3-pip python3-virtualenv git-core solr-jetty openjdk-8-jdk redis-server
	sudo apt-get install python3-venv
	sudo apt install libpq-dev python3-dev

2. AGREGRAR REPOS PARA LOS PAQUETES NECESARIOS EN CASO QUE NO SE ENCUENTREN EN LOS REPOS.
	-> OPENJDK-8
		sudo apt-get update
		sudo apt-get install software-properties-common
		sudo apt-add-repository 'deb http://security.debian.org/debian-security stretch/updates main'
		sudo apt-get update
		sudo apt-get install openjdk-8-jdk

	ALTERNATIVA DESCARGAR EL .TAR.GZ EN CASO DE NO ENCONTRAR FUENTES PARA OPENJDK-8
		https://download.java.net/openjdk/jdk8u41/ri/openjdk-8u41-b04-linux-x64-14_jan_2020.tar.gz

3. IMPORTANTE CAMBIAR VERSION PYTHON DEBE SER PYTHON 3.6 O SUPERIOR
	sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1


PASO 2. INSTALAR CKAN

1. CLONAR EL REPO
	git clone https://gitlab.com/comisiondelaverdad/sim-ckan.git

2. INSTALAR DESDE UN VIRTUAL ENVIROMENT

	sudo mkdir -p /usr/lib/ckan/sim-ckan
	sudo chown `whoami` /usr/lib/ckan/sim-ckan
	python3 -m venv /usr/lib/ckan/sim-ckan
	. /usr/lib/ckan/sim-ckan/bin/activate

3. CONFIFGURAR PIP
	pip install setuptools==44.1.0
	pip install --upgrade pip

4. INSTALAR CKAN
	pip install -e /PATHTOREPO
	LOCAL
	pip install -e /home/username/sim-ckan/

	SERVER
	pip install -e /home/administrador/sim-ckan/

5. INSTALAR LIBRERÍAS PYTHON DE CKAN.
	pip install -r requirements.txt

6. REACTIVAMOS EL VENV
	deactivate
	. /usr/lib/ckan/sim-ckan/bin/activate


PASO 3. CONFIGURAR POSTGRESS

1. INICIAR EL SERVICIO Y VERIFICARLO
	sudo service postgresql start
	sudo -u postgres psql -l

2. CREAR USUARIO CKAN Y DEFINIR PASSWORD
	sudo -u postgres createuser -S -D -R -P ckan_default
	pass: ckan_default

3. CREAR LA DB
	sudo -u postgres createdb -O ckan_default ckan_default -E utf-8


PASO 4. CREAR TABLAS DE CKAN

1. CREAR CONFIGURACION DE CKAN
	sudo mkdir -p /etc/ckan/sim-ckan
	sudo chown -R `whoami` /etc/ckan/
	ckan generate config /etc/ckan/sim-ckan/ckan.ini

2. EDITAR LA CONFIGURACION DE CKAN
	sudo nano /etc/ckan/sim-ckan/ckan.ini
		sqlalchemy.url = postgresql://ckan_default:pass@<remotehost>/ckan_default?sslmode=disable
		ckan.site_id = default
		solr_url = http://127.0.0.1:8983/solr
		ckan.site_url = http://demo.ckan.org


PASO 5. CONFIGURAR SOLR

1. CAMBIAR EL PUERTO DE JETTY/TOMCAT
	sudo nano /etc/jetty9/start.ini
	JETTY_PORT=8983

	sudo service jetty9 restart

2. COPIAR EL SCHEMA DE SOLR/CKAN
	LOCAL
	sudo mv /etc/solr/conf/schema.xml /etc/solr/conf/schema.xml.bak
	sudo ln -s /home/username/sim-ckan/ckan/config/solr/schema.xml /etc/solr/conf/schema.xml
	sudo service jetty9 restart

	SERVER
	sudo mv /etc/solr/conf/schema.xml /etc/solr/conf/schema.xml.bak
	sudo ln -s /home/administrador/sim-ckan/ckan/config/solr/schema.xml /etc/solr/conf/schema.xml
	sudo service jetty9 restart


PASO 6. CONFIGURAR RUTAS

1. LINK TO WHO.INI
	LOCAL
	ln -s /home/username/sim-ckan/who.ini /etc/ckan/sim-ckan/who.ini (con este no funcionó)
	ln -s /home/username/sim-ckan/ckan/config/who.ini /etc/ckan/sim-ckan/who.ini (con este sí funcionó)

	SERVER
	ln -s /home/administrador/sim-ckan/who.ini /etc/ckan/sim-cev/who.ini
	ln -s /home/administrador/sim-ckan/ckan/config/who.ini /etc/ckan/sim-ckan/who.ini


PASO 7. CREAR TABLAS DB

1. CREAR TABLAS
	LOCAL
	cd  /home/username/sim-ckan/ckan
	ckan -c /etc/ckan/sim-ckan/ckan.ini db init

	SERVER
	cd  /home/administrador/sim-ckan/ckan
	ckan -c /etc/ckan/sim-ckan/ckan.ini db init


PASO 8. INICIAR CKAN - PARA PRUEBAS

	sudo service redis-server start

	. /usr/lib/ckan/sim-ckan/bin/activate
	ckan -c /etc/ckan/sim-ckan/ckan.ini run

PASO 9. CONFIGURAR CKAN DEV COMO UN SERVICIO

1. CREAR EL ARCHIVO DEL SERVICIO
	sudo nano /etc/systemd/system/ckan.service

	[Unit]
	Description="RUN CKAN AS SERVICE"

	[Service]
	User=administrador
	Group=administrador
	WorkingDirectory=/usr/lib/ckan/sim-ckan/bin
	VIRTUAL_ENV=/usr/lib/ckan/sim-ckan/
	Environment=PATH=$VIRTUAL_ENV/bin:$PATH
	ExecStart=/usr/lib/ckan/sim-ckan/bin/ckan -c /etc/ckan/sim-ckan/ckan.ini run --host 0.0.0.0
	Restart=on-failure

	[Install]
	WantedBy=multi-user.target

2. INICIAR EL SERVICIO
	sudo service ckan start
