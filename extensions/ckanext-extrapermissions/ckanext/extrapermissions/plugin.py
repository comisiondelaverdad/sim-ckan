import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import ckan.model as model

def package_search(context, data_dict=None):
    return {'success': True, 'msg': 'No one is allowed to show package'}

def package_show(context, data_dict=None):
    convert_package_name_or_id_to_id = toolkit.get_converter('convert_package_name_or_id_to_id')
    package_id = convert_package_name_or_id_to_id(data_dict['id'], context)
    
    group_list = toolkit.get_action('group_list_authz')(data_dict={'am_member':True})
    group_package = toolkit.get_action('package_search')(data_dict={'include_private':True,'fq': 'id:{0}'.format(package_id)})

    grant = False
    if group_package['count'] == 1:
        group_list = [group['name'] for group in group_list]
        groups = [group['name'] for result in group_package['results'] for group in result['groups'] ]
        grant = any(group in group_list for group in groups)

    return {'success': grant, 'msg': 'No one is allowed to show package'}

def group_create(context, data_dict=None):
    return {'success': False, 'msg': 'Solo Administradores pueden crear Grupos'}

class ExtrapermissionsPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IAuthFunctions)
    
    def get_auth_functions(self):
        return {'package_show': package_show}
